import {Document, model, Model, Schema} from 'mongoose';
import {ObjectId} from "bson";

/**
 * HousingCrawled Interface and Model
 *
 * Feel free to edit both of it depending on your needs.
 */
export interface IHousingCrawled extends Document {
    _id: ObjectId,
    name: string,
    facilities: Array<string>,
    houseRules: Array<any>,
}

export const HousingCrawledSchema: Schema = new Schema({
    name: {type: String},
    facilities: {type: [String]},
    houseRules: {type: Schema.Types.Mixed}
}, {collection: 'housings.crawled', timestamps: true, autoIndex: true});

const HousingCrawledModel: Model<IHousingCrawled> = model<IHousingCrawled>('HousingCrawled', HousingCrawledSchema);
export default HousingCrawledModel;