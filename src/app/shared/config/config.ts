const env = process.env.NODE_ENV || 'default';

const config = {
    default: {
        name: 'default',
        database: {
            host: '127.0.0.1',
            port: '27035',
            db: 'EnterYourDatabaseHere',
            user: 'EnterYourUserHere',
            password: 'EnterYourPasswordHere'
        },
        webserver: {
            port: 3000
        }
    }
};

const Environment = config[env];
export {Environment};