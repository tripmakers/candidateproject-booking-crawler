import {Controller, Get, HttpStatus, Req, Res} from "@nestjs/common";
import {Query} from "@nestjs/common/utils/decorators/route-params.decorator";
import {CrawlerComponent} from "./crawler.component";
import {ApiUseTags, ApiOperation, ApiImplicitQuery} from "@nestjs/swagger";

@ApiUseTags('Crawler:Booking')
@Controller('api/v1/booking/crawler')
export class CrawlerController {

    constructor(private crawlerComponent: CrawlerComponent) {

    }

    @Get('/crawl-booking-page')
    @ApiOperation({title: 'Returns the extracted data of a crawled booking page and saves it to the database'})
    @ApiImplicitQuery({name: 'bookingUrl', description: 'The URL of the hotel we want to crawl'})
    async crawlBookingPage(@Res() res, @Req() req, @Query("bookingUrl") bookingUrl: string) {

        res.status(HttpStatus.OK).send({status: "ok"});
    }
}