import {Injectable} from "@nestjs/common";
import {HousingCrawledRepository} from "../../../shared/repositories/housing-crawled/housing-crawled.repository";

@Injectable()
export class ApiComponent {

    constructor(private housingCrawledRepository: HousingCrawledRepository) {

    }

}