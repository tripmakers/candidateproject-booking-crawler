import {Controller, Get, HttpStatus, Param, Req, Res} from "@nestjs/common";
import {ApiComponent} from "./api.component";
import {ApiImplicitParam, ApiOperation, ApiUseTags} from "@nestjs/swagger";

@Controller('api/v1/booking')
@ApiUseTags('Api:Booking')
export class ApiController {

    constructor(private apiComponent: ApiComponent) {
    }

    @Get('/housing-crawled/:housingId')
    @ApiOperation({title: 'Returns the saved data of an housing from our database'})
    @ApiImplicitParam({name: 'housingId', description: 'The id of the housing saved in our database'})
    async getHousingCrawledDetails(@Res() res, @Req() req, @Param("housingId") housingId: string) {
        res.status(HttpStatus.OK).send({status: "ok"});
    }
}